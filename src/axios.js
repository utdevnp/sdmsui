import axios from 'axios'

axios.defaults.baseURL = process.env.VUE_APP_API_URL; //'http://sdms.local:2052/api/';
axios.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem("token");


// Add a response interceptor
axios.interceptors.response.use(function (data) {

    return data;
    
  }, function (error) {
      if(error.response.data.message == "Unauthenticated." && error.response.status == 401){
        
        let tokenGrant = localStorage.getItem("token");
        if(tokenGrant){
            location.reload();
            console.log("Reloading......");
        }else{
            window.location.href = "http://"+location.host+"#/login";
        }
        
      }
    return Promise.reject(error);

  });



