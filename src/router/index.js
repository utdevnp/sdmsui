import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Newcetizen from '../views/Newcetizen.vue'
import Service from '../views/Service.vue'
import Serviceadd from '@/components/servicelist/Serviceadd.vue'
import Serviceupdate from '@/components/servicelist/Serviceupdate.vue'
import Loginpage from '../views/Loginpage.vue'
import Newcitizenshipadd from '@/components/citizenship/Newcitizenshipadd.vue'
import Newcitizenshiplist from '@/components/citizenship/Newcitizenshiplist.vue'
import Newcitizenshipupdate from '@/components/citizenship/Newcitizenshipupdate.vue'
import Fileuploadlist from '@/components/fileupload/Fileuploadlist.vue'
import Userlist from '@/components/user/Userlist.vue'
import Userdetail from '@/components/user/Userdetail.vue'

// admin imports
import AdminPage from "../views/AdminPage.vue"
import AdminDashboard from "@/components/admin/AdminDashboard.vue"
import Citizenshipview from "@/components/admin/citizenship/Citizenshipview.vue"
import Branchadd from "@/components/admin/branch/Branchadd.vue"
import Branchupdate from "@/components/admin/branch/Branchupdate.vue"
import Branchlist from "@/components/admin/branch/Branchlist.vue"
import Branchdetail from "@/components/admin/branch/Branchdetail.vue"
import Servicecenteradd from "@/components/admin/servicecenter/Servicecenteradd"
import Servicecenterlist from "@/components/admin/servicecenter/Servicecenterlist"
import Servicecenteredit from "@/components/admin/servicecenter/Servicecenteredit"
import Servicecenterdetail from "@/components/admin/servicecenter/Servicecenterdetail"
import Nameagedetailadmin from "@/components/admin/citizenchange/Nameagedetailadmin"
import Exforigenview from "../components/admin/exforigen/Exforigenview"
import Proofview from "../components/admin/proof/Proofview"
import Exarmyview from "../components/admin/exarmy/Exarmyview"
import Reliefview from "../components/admin/relief/Reliefview"
// service routers
import ServicePage from "../views/ServicePage"
import Nameagelist from "@/components/service/nameage/Nameagelist"
import Nameageadd from "@/components/service/nameage/Nameageadd"
import Nameageedit from "@/components/service/nameage/Nameageedit"
import Proofadd from "@/components/service/proof/Proofadd"
import Prooflist from "@/components/service/proof/Prooflist"
import Proofedit from "@/components/service/proof/Proofedit"
import Exarmyadd from "@/components/service/exarmy/Exarmyadd"
import Exarmyedit from "@/components/service/exarmy/Exarmyedit"
import Exarmylist from "@/components/service/exarmy/Exarmylist"
import Exforigenadd from '@/components/service/exforigen/Exforigenadd' 
import Exforigenedit from "@/components/service/exforigen/Exforigenedit"
import Exforigenlist from '@/components/service/exforigen/Exforigenlist'
import Relieflist from "@/components/service/relief/Relieflist"
import Reliefadd from "@/components/service/relief/Reliefadd"
import Reliefedit from '@/components/service/relief/Reliefedit'
import MinorAdd from "@/components/service/minor/MinorAdd"
import MinorList from "@/components/service/minor/MinorList"
import MinorEdit from "@/components/service/minor/MinorEdit"

// extra
import Showimage from "../views/Showimage"
import Report from "@/components/admin/dashboard/Report"


const routes = [
  
  { path: '/',    name: 'Home',    component: Home  },
  { path: '/about',   name: 'About',    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },

  { path: '/naya-nagarikta', name: 'Newcetizen', component : Newcetizen  },

  { path: '/servicelist', name: 'Service',    component: Service  },
  { path: '/servicelist/add', name: 'Serviceadd', component: Serviceadd  },
  { path: '/servicelist/edit/:id', name:'Serviceupdate', component: Serviceupdate },

  { path: '/login', name:'Loginpage', component: Loginpage },

  { path : '/newcitizenship-add', name:"Newcitizenshipadd", component: Newcitizenshipadd},
  { path : '/newcitizenship', name:"Newcitizenshiplist",component: Newcitizenshiplist},
  { path: '/newcitizenship/edit/:id', name:"Newcitizenshipupdate", component: Newcitizenshipupdate},
  { path: '/fileupload/:id/:type?', name:"Fileuploadlist", component:Fileuploadlist},

  { path:'/userlist', name:"Userlist", component:Userlist},
  { path:'/userdetail/:id', name:"Userdetail", component:Userdetail},
  { path:'/image/:querystring', name:'Showimage', component:Showimage},
  


  { path:"/admin", name:"Admin", component:AdminPage,

    children:[
      { path:"", component:AdminDashboard}, // for dashboard
      { path:"citizenship/:id", component:Citizenshipview},
      { path:"branch/add", name:"Branchadd", component:Branchadd},
      { path:"branch/update/:id", name:"Branchedit", component:Branchupdate},
      { path:"branch", name:'Branch', component:Branchlist},
      { path:'branch/detail/:id', name:'BranchDetail', component:Branchdetail},
      { path:'servicecenter/add', name:"Servicecenteradd", component:Servicecenteradd},
      { path:'servicecenter', name:"Servicecenter", component:Servicecenterlist},
      { path:'servicecenter/update/:id', name:"Servicecenteredit", component:Servicecenteredit},
      { path:'servicecenter/detail/:id', name:"Servicecenterdetail", component:Servicecenterdetail},
      { path:"citizenchange/detail/:id", name:"Nameagedetailadmin",component:Nameagedetailadmin},
      { path:"exforigen/detail/:id", name:"Exforigendetail", component:Exforigenview},
      { path:"proof/detail/:id", name:"Proofdetail", component:Proofview},
      { path:"exarmy/detail/:id", name:"Exarmydetail",component:Exarmyview},
      {path :"relief/detail/:id", name:"Reliefview", component:Reliefview}
     
    ]

  },

  {path:"/user", name:"User", component:ServicePage,
    children:[
      {path:"nameage", name:"Nameage", component:Nameagelist},
      {path:"nameage/add", name:"Nameageadd", component:Nameageadd},
      {path:"nameage/update/:id", name:"Nameageedit", component:Nameageedit},
      {path:"proof/add",name:"Proofadd",component:Proofadd},
      {path:"proof",name:"Prooflist",component:Prooflist},
      {path:"proof/update/:id", name:"Proofedit", component:Proofedit},
      {path:"exarmy/add", name:"Exarmyadd", component:Exarmyadd},
      {path:"exarmy/update/:id", name:"Exarmyedit", component:Exarmyedit},
      {path:"exarmy", name:"Exarmylist", component:Exarmylist},
      {path:"exforigen/add", name:"Exforigenadd", component:Exforigenadd},
      {path:"exforigen/update/:id", name:"Exforigenedit", component:Exforigenedit},
      {path:"exforigen", name:"Exforigenlist", component:Exforigenlist},
      {path:"relief", name:"Relieflist", component:Relieflist},
      {path: "relief/add", name:"Reliefadd", component:Reliefadd},
      {path: "relief/update/:id", name:"Reliefedit", component:Reliefedit},
      {path:"minor/add", name:"MinorAdd", component:MinorAdd},
      {path:'minor', name:"MinorList", component:MinorList},
      {path:'minor/update/:id', name:'MinorEdit', component:MinorEdit},

        // report path 
      {path:'/report',name:'Report', component:Report},
    
    ]
  }

] 

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
