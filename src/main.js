import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
import jQuery from 'jquery'
global.jQuery = jQuery

//import LogRocket from 'logrocket'
//LogRocket.init('x6sxag/sdms-ui')
import './axios'


//let appApiUrl = process.env.VUE_APP_API_URL+"api/"
// Vue.prototype.$appImageUrl = process.env.VUE_APP_API_URL+"assets/images/"

createApp(App).use(router,jQuery).mount('#app')


